package com.capuccino.tulip.exception;


public class MysqlDBException extends Exception {

	
	private static final long serialVersionUID = 8450585532597136531L;

	public MysqlDBException(){
		
	}
	
	public MysqlDBException(String message){
		super(message);
	}
	
}
