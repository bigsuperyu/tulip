## mybatis封装类库

#### 相关介绍
1. 单体DAO自动完成增删改查，无需XML
2. BaseService 实现通用的查询方法

#### 演示 Demo
```
Query<Demo> query = Query.build(Demo.class);
query.setPaged(pageNo, pageSize);
query.addLike("title", title);
query.addEq("status", NoticePO.YES);
query.addOrder("createTime", DBOrder.DESC);
```

#### 配置说明
```
 <bean id="sqlSessionFactory" class="org.mybatis.spring.SqlSessionFactoryBean"> 
     <property name="dataSource" ref="dataSource"/> 
     <property name="typeAliasesPackage" value="com.demo.*.pojo"/> 
 </bean> 
 <bean class="org.mybatis.spring.mapper.MapperScannerConfigurer"> 
     <property name="basePackage" value="com.demo.*.dao"/> 
 </bean> 
```